CREATE DATABASE Practica2017;
USE Practica2017;

/*
LAS INSTRUCCIONES ESTAN EN TODO EL DOCUMENTO DDL.
1. NO SE DEBE DE ALTERAR LA ESTRUCTURA DE LA BASE DE DATOS DADA.
2. CREAR EL ARCHIVO CORRESPONDIENTE DE DML PARA LA BASE DE DATOS.
3. EN EL ARCHIVO DML UTILIZAR TODOS LOS PROCEDIMIENTOS DECLARADOS EN EL ARCHIVO DDL.
4. SOLO DE DEBE DE CREAR LOS PROCEDIMIENTOS DE ALMACENADO INDICADOS.
*/


CREATE TABLE Tarea(
		idTarea INT NOT NULL AUTO_INCREMENT,
    titulo VARCHAR(30) NOT NULL,
    descripcion VARCHAR(40) NOT NULL,
		fecha_registro DATETIME NOT NULL,
		fecha_final DATETIME NOT NULL,
    PRIMARY KEY (idTarea)
);

CREATE TABLE Usuario(
		idUsuario INT NOT NULL AUTO_INCREMENT,
    nick VARCHAR(30) NOT NULL,
    contrasena VARCHAR(30) NOT NULL,
    cambios_contrasena INT NOT NULL,
		fecha_registro DATETIME NOT NULL,
    fecha_modificacion DATETIME NULL,
		picture TEXT NULL,
    PRIMARY KEY (idUsuario)
);

CREATE TABLE UsuarioTareas(
		idUsuarioTareas INT NOT NULL AUTO_INCREMENT,
    idUsuario INT NOT NULL,
    idTarea INT NOT NULL,
    PRIMARY KEY (idUsuarioTareas),
    FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario),
    FOREIGN KEY (idTarea) REFERENCES Tarea(idTarea)
);

DELIMITER $$
CREATE PROCEDURE sp_insertUsuario(
_nick VARCHAR(30),
_contrasena VARCHAR(30))
BEGIN
	INSERT INTO Usuario(nick ,contrasena ,cambios_contrasena, fecha_registro ,fecha_modificacion ,picture)
    VALUES(_nick ,_contrasena, 0, NOW(), NOW(), 'http://icon-icons.com/icons2/317/PNG/512/user-male-alt-icon_34333.png');
END $$

/*
SP USUARIO UPDATE
CREAR UN PROCEDIMIENTO DE ALMACENADO QUE REALICE LO SIGUIENTE:
1. Modificar el usuario que espere los siguientes parametros:
a. nick
b. contrasena
c. picture
d. idUsuario
2. si se modifica la contraseña llevar el control de los cambios de contraseña
3. actualizar la fecha de modificacion
*/
CREATE PROCEDURE sp_usuarioUPDATE(
  _nick VARCHAR(30),
  _contrasena VARCHAR(30),
  _picture TEXT,
  _idUsuario INT)
BEGIN
	DECLARE _noCambios INT;
  SET _noCambios = (SELECT cambios_contrasena FROM Usuario WHERE idUsuario = _idUsuario LIMIT 1) +1;

  UPDATE Usuario SET nick = _nick , contrasena = _contrasena, picture = _picture,
  fecha_modificacion = NOW(), cambios_contrasena =_noCambios WHERE idUsuario = _idUsuario; 
END $$

/*
SP_AUTENTICAR
*/

CREATE PROCEDURE sp_autenticarUsuario(
  _nick VARCHAR(30),
  _contrasena VARCHAR(30))
BEGIN
	SELECT * FROM Usuario WHERE nick = _nick AND contrasena = _contrasena LIMIT 1;
END $$

/*
SP USUARIO SELECT
CREAR UN PROCEDIMIENTO DE ALMACENADO QUE REALICE LO SIGUIENTE:
1. mostrar todos los usuarios con sus datos
2. la consulta debe de llevar lo siguiente:
a. idUsuario
b. nick
c. contrasena
d. cambios_contrasena
e. fecha_registro
f. hora_registro
g. fecha_modificacion
h. hora_modificacion
i. picture
3. Debe de estar ordenada de los usuarios que han sido modificados ultimamente.
*/
CREATE PROCEDURE sp_UsuarioSELECT()
BEGIN
  SELECT *, TIME(fecha_registro) AS hora_registro, TIME(fecha_modificacion) AS hora_modificacion 
  FROM Usuario ORDER BY fecha_modificacion DESC;
END $$  
/*
SP USUARIO DELETE
CREAR UN PROCEDIMIENTO DE ALMACENADO QUE REALICE LO SIGUIENTE:
1. Eliminar el usuario
2. Eliminar todas las tareas que tenga asignada
*/
CREATE PROCEDURE sp_UsuarioDELETE(
  _idUsuario INT
)
BEGIN
  DELETE FROM UsuarioTareas WHERE idUsuario = _idUsuario;
  DELETE FROM Usuario WHERE idUsuario = _idUsuario;
END $$  
/*
SP TAREA INSERT
CREAR UN PROCEDIMIENTO DE ALMACENADO QUE REALICE LO SIGUIENTE:
1. Registrar una nueva tarea junto al usuario quien lo creo
*/
CREATE PROCEDURE sp_TareaINSERT(
  _titulo VARCHAR(30),
  _descripcion VARCHAR(40),
  _fecha_final DATETIME,
  _idUsuario INT 
)
BEGIN
  DECLARE _idTarea INT;
  INSERT INTO Tarea(titulo, descripcion, fecha_registro, fecha_final)
  VALUES (_titulo, _descripcion, NOW(), _fecha_final);
  SET _idTarea = (SELECT idTarea FROM Tarea WHERE titulo= _titulo && descripcion= _descripcion && fecha_final = _fecha_final); 
  INSERT INTO UsuarioTareas (idUsuario, idTarea)
  VALUES(_idUsuario,_idTarea );
END $$  
/*
SP TAREA SELECT
CREAR UN PROCEDIMIENTO DE ALMACENADO QUE REALICE LO SIGUIENTE:
1. Mostrar todas las tareas del usuario por medio de su id
2. Mostrar en la consulta las siguientes columnas.
a. idTarea
b. titulo
c. descripcion
d. fecha_registro
e. hora del registro
f. fecha_final
g. hora final
3. la consulta debe de estar ordenada por fecha y hora.
*/

CREATE PROCEDURE sp_TareaSELECT(
  _idUsuario INT 
)
BEGIN
  SELECT *, TIME(fecha_registro) AS hora_registro, TIME(fecha_final) AS hora_final 
  FROM Tarea WHERE idUsuario = _idUsuario 
  ORDER BY fecha_final DESC ;
END $$ 