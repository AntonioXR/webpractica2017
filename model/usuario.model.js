var database = require('../config/database.config');
var usuario = {};

usuario.find = function(idUsuario, callback) {
  if(database) {
    database.query(`
      SELECT *, TIME(fecha_registro) AS hora_registro, TIME(fecha_modificacion) AS hora_modificacion 
      FROM Usuario WHERE idUsuario = ? ORDER BY fecha_modificacion DESC;
    `, idUsuario, function(error, resultados) {
      if(error){ throw error;}
      if(resultados.length > 0) {
        callback(resultados);
      } else {
        callback(0);
      }
    })
  }
}


usuario.selectAll = function(callback) {
  if(database) {
    database.query('CALL sp_UsuarioSELECT;',
    function(error, resultados) {
      if(error){ throw error;}
      if(resultados.length > 0) {
        callback(resultados);
      } else {
        callback(0);
      }
    })
  }
}

usuario.insert = function(data, callback) {
  if(database) {
    database.query("CALL sp_insertUsuario(?,?)", data,
    function(error, resultado) {
    if(error){ throw error;}
      callback({Mensaje: true});
    });
  }
}

usuario.update = function(data, callback){
  if(database) {
    database.query("CALL sp_usuarioUPDATE(?,?,?)",data,
    function(error, resultado) {
      if(error){ throw error;}
      callback({Mensaje: true});
    });
  }
}
usuario.delete = function(idUsuario, callback){
  if(database) {
    database.query("CALL sp_UsuarioDELETE(?)",idUsuario,
    function(error, resultado) {
      if(error){ throw error;}
      callback({Mensaje: true});
    });
  }
}

module.exports = usuario;
