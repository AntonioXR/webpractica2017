var database = require('../config/database.config');
var tarea = {};

tarea.selectALL = function(callback){
  if(database) {
    database.query(`
     SELECT *, TIME(fecha_registro) AS hora_registro, TIME(fecha_final) AS hora_final 
    FROM Tarea ORDER BY fecha_final DESC ;
    `, function(error, resultados) {
      if(error){ throw error;}
      if(resultados.length > 0) {
        callback(resultados);
      } else {
        callback(0);
      }
    })
  }
}

tarea.find = function(idTarea,callback){
  if(database) {
    database.query(`
     CALL sp_TareaSELECT(?) ;
    `, idTarea, function(error, resultados) {
      if(error){ throw error;}
      if(resultados.length > 0) {
        callback(resultados);
      } else {
        callback(0);
      }
    })
  }
}

tarea.insert = function(data,callback){
  if(database) {
    database.query(`
     CALL sp_TareaINSERT(?,?,?,?) ;
    `, data, function(error, resultados) {
      if(error){ throw error;}
      if(resultados.length > 0) {
        callback(resultados);
      } else {
        callback(0);
      }
    })
  }
}
module.exports = tarea;