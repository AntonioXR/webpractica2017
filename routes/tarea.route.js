var express = require('express');
var router = express.Router();
var tarea = require("../model/tarea.model");
// middleware that is specific to this router
router.route('/tarea')
  .get(function(req, res){

    tarea.selectALL(function(response){
      if(response != null){
        res.json(response)
      }
    })

  }).post(function(req,res){

    var data = [req.body.titulo,req.body.descripcion,req.body.fechaFinal,req.body.idUsuario]
    tarea.insert(data,function(response){
      if(response != null){
        res.json(response)
      }
    })

  });


router.get('/tarea/:idTarea', function(req, res) {

    var data = req.body.idTarea
    tarea.find(data,function(response){
      if(response != null){
        res.json(response)
      }
    })

});



module.exports = router;
